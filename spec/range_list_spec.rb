require "range_list"

describe RangeList do 
  it "init with no params" do 
    rl = RangeList.new
    expect(rl.ranges.length).to eq(0)
  end

  it "add nil" do 
    rl = RangeList.new
    rl.add([1, 5])
    rl.add(nil)
    expect(rl.ranges.length).to eq(1)
  end

  it "add invalid range" do 
    rl = RangeList.new
    rl.add([1, 5])
    rl.add([10])
    expect(rl.ranges).to eq([[1,5]])
  end

  it "add invalid other object" do 
    rl = RangeList.new
    rl.add([1, 5])
    rl.add(1)
    expect(rl.ranges).to eq([[1,5]])
  end

  it "add valid range" do 
    rl = RangeList.new
    rl.add([1,5])
    expect(rl.ranges).to eq([[1,5]])
  end

  it "add range multiple times" do 
    rl = RangeList.new
    rl.add([1,5])
    rl.add([10,30])
    rl.add([3, 8])
    expect(rl.ranges).to eq([[1,8], [10,30]])
  end

  it "chain add" do 
    rl = RangeList.new
    rl.add([1, 5]).add([10, 20]).add([4, 8])
    expect(rl.ranges).to eq([[1,8],[10,20]])
  end

  it "remove invalid range" do
    rl = RangeList.new
    rl.add([1, 5]).add([10,20])
    rl.remove(2)
    expect(rl.ranges).to eq([[1,5],[10,20]])
  end

  it "remove nil" do
    rl = RangeList.new
    rl.add([1, 5]).add([10,20])
    rl.remove(nil)
    expect(rl.ranges).to eq([[1,5],[10,20]])
  end

  it "remove range" do 
    rl = RangeList.new
    rl.add([1, 5]).add([10,20]).add([4,8])
    rl.remove([2, 5])
    expect(rl.ranges).to eq([[1,2],[5,8], [10,20]])
  end

  it "chain remove range" do 
    rl = RangeList.new
    rl.add([1, 5]).add([10,20]).add([4,8])
    rl.remove([2, 5]).remove([14, 21])
    expect(rl.ranges).to eq([[1,2],[5,8], [10,14]])
  end

  it "chain with add and remove" do 
    rl = RangeList.new
    rl.add([1, 5]).remove([10,20]).add([4,8]).add([14, 21]).remove([3, 15])
    expect(rl.ranges).to eq([[1,3],[15,21]])
  end

  it "clear the ranges" do 
    rl = RangeList.new
    rl.add([1, 5]).remove([10,20]).add([4,8]).add([14, 21]).remove([3, 15])
    rl.clear
    expect(rl.ranges.length).to eq(0)
  end

  it "print all ranges" do 
    rl = RangeList.new
    rl.add([1, 5]).add([4,8]).add([14, 21])
    expect(rl.print).to eq("[1, 8) [14, 21)")
  end
end