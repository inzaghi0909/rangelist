##
# A pair of integers define a range, for example: [1, 5). This range includes integers: 1, 2, 3, and 4.
#
# A range list is an aggregate of these ranges: [1, 5), [10, 11), [100, 201)
class RangeList

  # all ranges (2D array). sorted by the item's first integer
  attr_reader :ranges
  
  # create a new range list. the ranges is an empty array
  def initialize
      @ranges = []
  end

  # add range to the ranges. +new_range+ should be an array with two integers that specify beginning and the end of the new_range. 
  #
  # return +self+, so can be operation with chain. like add([1, 2]).add([4, 6])
  def add(new_range)
    return self if new_range.nil? || !new_range.is_a?(Array)
    return self if new_range.size != 2 

    left = new_range[0]
    right = new_range[1]

    return self if left >= right

    temp_ranges = []
    pushed = false
    @ranges.each do |range|
      if left > range[1]
        # on the current range right
        temp_ranges << range        
      elsif range[0] > right
        # on the current range left
        temp_ranges << [left, right]
        temp_ranges << range
        pushed = true
      else
        # intersection
        left = [range[0], left].min 
        right = [range[1], right].max
      end
    end
    temp_ranges << [left, right] unless pushed

    @ranges = temp_ranges
    self
  end

  # remove a range from the ranges. +range+ should be an array with two integers that specify beginning and the end of the new_range. 
  #
  # return +self+, so can be operation with chain. like remove([1, 2]).remove([4, 6])
  def remove(range)
    return self if range.nil? || !range.is_a?(Array)
    return self if range.size != 2 

    left = range[0]
    right = range[1]

    return self if left >= right

    temp_ranges = []
    @ranges.each do |inner_range|
      if inner_range[0] > right || inner_range[1] < left
        # no interaction
        temp_ranges << inner_range
        next
      end

      if inner_range[0] >= left && inner_range[1] <= right 
        # all remove
        next
      end

      if left > inner_range[0]
        temp_ranges << [inner_range[0], left]
      end
      if right < inner_range[1]
        temp_ranges << [right, inner_range[1]]
      end
    end
    
    @ranges = temp_ranges
    self
  end

  # clear all ranges. return self.
  def clear 
    @ranges.clear 
    self
  end

  # print all of the range in the ranges
  def print
    pp @ranges.map {|r| "[#{r[0]}, #{r[1]})" }.join(' ')
  end  

end

# rl = RangeList.new()
# pp '*' * 30

# rl.add([1, 5])
# rl.print
# # Should display: [1, 5)
# rl.add([10, 20])
# rl.print
# # Should display: [1, 5) [10, 20)
# rl.add([20, 20])
# rl.print
# #Should display: [1, 5) [10, 20)
# rl.add([20, 21])
# rl.print
# # Should display: [1, 5) [10, 21)

# rl.add([2, 4])
# # Should display: [1, 5) [10, 21)
# rl.add([3, 8])
# rl.print
# # Should display: [1, 8) [10, 21)

# pp '=' * 40

# rl.remove([10, 10])
# rl.print

# rl.remove([10, 11])
# rl.print
# # Should display: [1, 8) [11, 21)
# rl.remove([15, 17])
# rl.print
# # Should display: [1, 8) [11, 15) [17, 21)
# rl.remove([3, 19])
# rl.print

# rl.remove([30, 59])
# rl.print

# Should display: [1, 3) [19, 21)